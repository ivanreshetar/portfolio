'use strict';

module.exports = function() {
    $.gulp.task('sprite:gif', function() {
        return $.gulp.src('./source/sprites/*.gif')
            .pipe($.gp.spritesmith({
                imgName: 'sprite.gif',
                cssName: 'css/sprite_gif.css'
            }))
            .pipe($.gulp.dest($.config.root + '/assets/img'))
    })
};